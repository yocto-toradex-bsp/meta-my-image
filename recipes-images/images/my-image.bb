SUMMARY = "Embedded Linux Console Image"

LICENSE = "MIT"

PV = "${TDX_VER_INT}"
export IMAGE_BASENAME = "my-image"
IMAGE_NAME_colibri-imx6 = "Colibri-iMX6_${IMAGE_BASENAME}"
IMAGE_NAME_colibri-imx7 = "Colibri-iMX7_${IMAGE_BASENAME}"
IMAGE_NAME = "${MACHINE}_${IMAGE_BASENAME}"

require recipes-images/images/tdx-image-fstype.inc

IMAGE_LINGUAS = " "

IMAGE_INSTALL += " \
    packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL} \
    packagegroup-base-usbhost \
    packagegroup-base-usbgadget \
    libusbgx \
    libusbgx-examples \
"
EXTRA_IMAGE_FEATURES = "debug-tweaks tools-debug"

# Extra target features
# - Stack smash prototection libs
TOOLCHAIN_TARGET_TASK_append = " libssp-dev"

# Extra host tools
TOOLCHAIN_HOST_TASK_append = " nativesdk-cmake"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "131072"

DISTRO_FEATURES_remove = "ipv6"
